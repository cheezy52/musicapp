MusicApp::Application.routes.draw do
  get '/users/activate', to: "users#activate"
  resources :users, only: [:new, :create, :show, :index, :update] do
    patch :make_admin, to: "users#make_admin!"
    collection do
      get :activate
    end
  end
  resource :session, only: [:new, :create, :destroy]
  resources :bands do
    resources :albums, only: [:new, :create]
  end
  resources :albums, only: [:edit, :update, :show, :destroy] do
    resources :tracks, only: [:new, :create]
  end
  resources :tracks, only: [:edit, :update, :show, :destroy] do
    resources :notes, only: [:create]
  end
  resources :notes, only: [:destroy]
  root to: 'bands#index'
end
