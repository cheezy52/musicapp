class TracksController < ApplicationController
  before_action :redirect_unless_admin, only: [:new, :edit, :create, :update, :destroy]

  def index
    @tracks = Track.all
    render :index
  end

  def show
    @track = Track.find(params[:id])
    @album = @track.album
    @band = @track.band
    @notes = @track.notes
    @note = Note.new(:track_id => @track.id, :user_id => current_user.id)
    render :show
  end

  def new
    @albums = Album.all
    @track = Track.new
    render :new
  end

  def create
    @albums = Album.all
    @track = Track.new(track_params)
    if @track.save
      redirect_to album_url(@track.album)
    else
      flash.now[:errors] = @track.errors.full_messages
      render :new
    end
  end

  def edit
    @albums = Album.all
    @track = Track.find(params[:id])
    render :edit
  end

  def update
    @albums = Album.all
    @track = Track.find(params[:id])
    @track.update_attributes(track_params)
    if @track.save
      redirect_to track_url(@track)
    else
      flash.now[:errors] = @track.errors.full_messages
      render :edit
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @album.destroy!
    redirect_to band_albums_url(@album.band_id)
  end

  private
  def track_params
    params.require(:track).permit(:name, :album_id, :lyrics, :bonus)
  end
end
