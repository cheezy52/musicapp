class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  def admin?
    if current_user
      return current_user.admin
    end
    false
  end

  def redirect_unless_admin
    redirect_to bands_url unless admin?
  end
end
