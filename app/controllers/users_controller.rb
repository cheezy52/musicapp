class UsersController < ApplicationController
  before_action :redirect_unless_admin, only: [:index, :make_admin!]

  def index
    @users = User.all
    render :index
  end

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.activation_token = SecureRandom::urlsafe_base64(16)
      @user.save
      conf_email = UserMailer.signup_confirmation_email(@user)
      conf_email.deliver!
      flash[:notice] = "Confirmation e-mail sent!  Please follow the link therein."
      redirect_to "/"
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
    render :show
  end

  def activate
    @user = User.find_by_activation_token(params[:activation_token])
    if @user
      if @user.activated == false
        @user.activated = true
        @user.save!
        flash[:notice] = "Signup complete.  Welcome aboard!"
        login!(@user)
        redirect_to user_url(@user)
      else
        flash[:errors] = "You've already finished activation of this account!"
        redirect_to user_url(@user)
      end
    else
      flash[:errors] = "This activation key does not match any accounts."
      redirect_to new_user_url
    end
  end

  def make_admin!
    @user = User.find(params[:user_id])
    @user.admin = true
    @user.save!
    redirect_to users_url
  end

  private
  def user_params
    params.require(:user).permit(:email, :password)
  end
end
