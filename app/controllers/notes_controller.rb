class NotesController < ApplicationController
  #def new
  #  @note = Note.new
  #  render :new
  #end

  def create
    @note = Note.new(note_params)
    @note.track_id = params[:track_id]
    @note.user_id = current_user.id
    if @note.save
      redirect_to track_url(params[:track_id])
    else
      flash.now[:errors] = @note.errors.full_messages
      redirect_to track_url(params[:track_id])
    end
  end

  def destroy
    if admin?
      @note = Note.find(params[:id])
      @note.destroy!
      redirect_to track_url(@note.track)
    else
      flash[:errors] = "You do not have access privileges for that action"
      redirect_to bands_url
    end
  end

  private
  def note_params
    params.require(:note).permit(:text, :track_id, :user_id)
  end
end
