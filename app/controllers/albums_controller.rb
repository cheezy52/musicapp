class AlbumsController < ApplicationController
  before_action :redirect_unless_admin, only: [:new, :edit, :create, :update, :destroy]

  def index
    @albums = Album.all
    render :index
  end

  def show
    @album = Album.find(params[:id])
    @band = @album.band
    @tracks = @album.tracks
    render :show
  end

  def new
    @album = Album.new
    @bands = Band.all
    render :new
  end

  def create
    @album = Album.new(album_params)
    @bands = Band.all
    if @album.save
      redirect_to band_url(params[:band_id])
    else
      flash.now[:errors] = @album.errors.full_messages
      render :new
    end
  end

  def edit
    @album = Album.find(params[:id])
    @bands = Band.all
    render :edit
  end

  def update
    @album = Album.find(params[:id])
    @bands = Band.all
    @album.update_attributes(album_params)
    if @album.save
      redirect_to album_url(@album)
    else
      flash.now[:errors] = @album.errors.full_messages
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @album.destroy!
    redirect_to albums_url
  end

  private
  def album_params
    params.require(:album).permit(:name, :band_id, :live)
  end
end
