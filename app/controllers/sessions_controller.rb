class SessionsController < ApplicationController
  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.find_by_credentials(user_params)
    if @user
      if @user.activated
        login!(@user)
        redirect_to user_url(@user)
      else
        flash[:errors] = "You have not completed signup.  Please activate your account via the link sent to your e-mail address."
        redirect_to new_session_url
      end
    else
      flash.now[:errors] = "No user matches this username/password combo."
      @user = User.new(user_params)
      render :new
    end
  end

  def destroy
    @user = current_user
    logout!(@user)
    redirect_to new_session_url
  end

  private
  def user_params
    params.require(:user).permit(:email, :password)
  end
end
