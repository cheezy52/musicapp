# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)      not null
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  validates :email, :password_digest, :session_token, :presence => true
  validates :email, :session_token, :uniqueness => true
  before_validation :init

  has_many :notes

  def password=(plaintext)
    write_attribute(:password_digest, BCrypt::Password.create(plaintext))
  end

  def is_password?(plaintext)
    BCrypt::Password.new(password_digest).is_password?(plaintext)
  end

  def reset_session_token!
    write_attribute(:session_token, generate_session_token)
  end

  def self.find_by_credentials(params)
    u = User.find_by_email(params[:email])
    return u if u && u.is_password?(params[:password])
    nil
  end

  private
  def init
    self.session_token ||= generate_session_token
    self.activated ||= false
    self.admin ||= false
    self.activation_token ||= SecureRandom::urlsafe_base64(16)
  end

  def generate_session_token
    SecureRandom::urlsafe_base64(16)
  end
end
