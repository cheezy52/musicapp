class Track < ActiveRecord::Base
  validates :name, :album_id, :presence => true

  belongs_to :album
  has_one :band, :through => :album
  has_many :notes
end
