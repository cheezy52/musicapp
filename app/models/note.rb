class Note < ActiveRecord::Base
  validates :text, :user_id, :track_id, :presence => true

  belongs_to :track
  belongs_to :user
end
