class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def signup_confirmation_email(user)
    @user = user
    @url = "localhost:3000/users/activate?activation_token=#{@user.activation_token}"
    mail(to: user.email, subject: 'Confirm signup for Music App')
  end
end
