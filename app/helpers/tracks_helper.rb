module TracksHelper
  def ugly_lyrics(lyrics)
    lined_lyrics = h(lyrics).split("\n")
    noted_lyrics = "&#9835;#{lined_lyrics.join('&#9835;')}".html_safe
    noted_lyrics
  end
end
