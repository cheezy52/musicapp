module ApplicationHelper
  def csrf_token
    "<input type='hidden' name='authenticity_token' value='#{form_authenticity_token}'>".html_safe
  end

  def request_field(method)
    "<input type='hidden' name='_method' value='#{method}'".html_safe
  end

  def admin?
    if current_user
      return current_user.admin
    end
    false
  end
end
